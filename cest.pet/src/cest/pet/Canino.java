package cest.pet;

public abstract class Canino extends Animal {
	public Canino(String nome, int idade, boolean doente) {
		super(nome, idade, doente);
		// TODO Auto-generated constructor stub
	}

	public void fazerBarulho() {
		if (getIdade() >= 0) {
			System.out.println("Au, Ao!");
		} else {
			System.out.println("Não late");
		}
	}

	public void latir() {
		if (isDoente() == true) {
			System.out.println("Não late, pois está doente.");
		} else {
			System.out.println("late!!");
		}
	}
}
