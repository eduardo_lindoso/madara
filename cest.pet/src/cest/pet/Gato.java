package cest.pet;

public class Gato extends Felino {
	public Gato(String nome, int idade, boolean doente) {
		super(nome, idade, doente);
		// TODO Auto-generated constructor stub
	}

	public void procurarComida() {
		if (isDoente() == true) {
			System.out.println("Não procura comida, pois, está doente");
		} else {
			System.out.println("Caça os ratinhos e baratas de casa");
		}
	}

	public void seEsfregarNasPedras() {
		if (isDoente() == true) {
			System.out.println("Não se esfrega nas pedras, pois, está doente");
		} else {
			System.out.println("Se esfrega nas pedras");
		}

	}
}
