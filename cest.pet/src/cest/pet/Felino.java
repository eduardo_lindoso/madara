package cest.pet;

public abstract class Felino extends Animal {
	public Felino(String nome, int idade, boolean doente) {
		super(nome, idade, doente);
		// TODO Auto-generated constructor stub
	}

	public void fazerBarulho() {
		if (getIdade() >= 0) {
			System.out.println("Miau, Miau!");
		} else {
			System.out.println("Não mia");
		}
	}

	public void miar() {
		if (isDoente() == true) {
			System.out.println("Não mia, pois está doente.");
		} else {
			System.out.println("Mia a noite toda no telhado");
		}

	}
}
