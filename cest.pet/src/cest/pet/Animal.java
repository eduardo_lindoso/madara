package cest.pet;

public abstract class Animal {
	private String nome;
	private int idade;
	private TipoRaca raca;
	private boolean doente;

	public Animal(String nome, int idade, boolean doente) {
		this.setNome(nome);
		this.setIdade(idade);
		this.setDoente(doente);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public boolean isDoente() {
		return doente;
	}

	public void setDoente(boolean doente) {
		this.doente = doente;
	}

	public void dormir() {
		if (doente == true) {
			System.out.println("Precisa dormir");
		} else {
			System.out.println("Não precisa dormir, ta na hora de caçar");
		}
	}

	public void fazerBarulho() {

	}

	public void procuraComida() {

	}

	public TipoRaca getRaca() {
		return raca;
	}

	public void setRaca(TipoRaca raca) {
		this.raca = raca;
	}
}
